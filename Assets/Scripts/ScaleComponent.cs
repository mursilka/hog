using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
public class ScaleComponent : FindComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleDuration;

    public override void DoEffect(Action callback)
    {
        transform.DOScale(transform.localScale * _scaleMultiplier, _scaleDuration).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }

}
