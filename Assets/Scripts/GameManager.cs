using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
   [SerializeField] private UIManager _uIManager;
   [SerializeField] private List<Level> _levels = new List<Level>();

    private Level _currentLevel;
    private int _levelIndex;

    public UnityEvent<string> OnItemChange;

    private void Start()
    {
        _uIManager.ShowStartScreen();
        
    }

    private void CreateLevel()
    {
        if (_currentLevel != null)
        {           
            Destroy(_currentLevel.gameObject);
            _currentLevel = null;
        }

        int index = _levelIndex;
        if (_levelIndex >= _levels.Count)
        {
            index = _levelIndex % _levels.Count;
        }

        _currentLevel= Instantiate(_levels[index].gameObject).GetComponent<Level>();
    }

    public void StartGame()
    {
        CreateLevel();
        _currentLevel.OnComplite += StopGame;
        _currentLevel.OnItemListChanged += OnItemListChanged;
        _currentLevel.Initialize();
        _uIManager.ShowGameScreen(_currentLevel.GetItemDictionary());
    }
    private void StopGame()
    {
        _levelIndex++;
        _currentLevel.OnComplite -= StopGame;
        _currentLevel.OnItemListChanged -= OnItemListChanged;
        _uIManager.ShowWinScreen();
    }

    public void ExitGame()  
    { 
        Application.Quit();
    }

    private void OnItemListChanged(string name) => OnItemChange?.Invoke(name);
}
