using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class Level : MonoBehaviour
{
    

    private List<GameItem> _items= new List<GameItem>();
    private int _itemsCount;
    

    public event Action OnComplite;
    public event Action <string> OnItemListChanged;

    public void Initialize()
    {
        GameItem[] gameItems = GetComponentsInChildren<GameItem>();
        _items.AddRange(gameItems);
        _itemsCount= _items.Count;
        for (int i = 0; i < _items.Count; i++)
        {
            _items[i].OnFind += OnItemFinded;
        }
    }

    private void OnItemFinded(string _name)
    {
        _itemsCount--;

        OnItemListChanged?.Invoke(_name);

        if( _itemsCount == 0)
        {
            Debug.Log("Level complited");   
            OnComplite?.Invoke();                                            
        }
    }

    public Dictionary<string, GameItemData> GetItemDictionary()
    {
        Dictionary<string, GameItemData> dict = new Dictionary<string, GameItemData>();

        for(int i = 0; i < _items.Count; i++)
        {
            if (dict.ContainsKey(_items[i].Name) == true)
            {
                dict[_items[i].Name].IncreaseCount();
            }
            else
            {
                dict.Add(_items[i].Name, new GameItemData(_items[i].Sprite));
            }
        }
        return dict;
    }
    
}
