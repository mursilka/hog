using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class TurnComponent : FindComponent
{
    [SerializeField] private float _rotationMultiplier;
    [SerializeField] private float _rotationDuration;

    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public override void DoEffect(Action callback)
    {
        transform.DORotate(new Vector3(0f, 0f, _rotationMultiplier), _rotationDuration, RotateMode.FastBeyond360).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
            
        });
    }
}
